<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    header('Location: //regioopleiders.nl', true, 307);
    exit;
});

Auth::routes();

Route::any('/tool', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/logout', [App\Http\Controllers\Auth\LogoutController::class, 'logout'])->name('logout');

Route::get('/users', [\App\Http\Controllers\Roles::class, 'list'])->name('users');
Route::get('/user/{id}/{action}', [\App\Http\Controllers\Roles::class, 'save']);

Route::get('/aanmelden', [\App\Http\Controllers\RegistrationController::class, 'form'])->name('aanmelden');
Route::post('/aanmelden', [\App\Http\Controllers\RegistrationController::class, 'save'])->name('aanmelden');

Route::get('/event/new', [\App\Http\Controllers\EventController::class, 'new'])->name('new_event');
Route::post('/event/new', [\App\Http\Controllers\EventController::class, 'create'])->name('new_event');
Route::get('/event/{id}', [\App\Http\Controllers\EventController::class, 'view'])->name('event');
Route::get('/edit/{id}', [\App\Http\Controllers\EventController::class, 'edit'])->name('edit_event');
Route::post('/save', [\App\Http\Controllers\EventController::class, 'save'])->name('save_event');
Route::get('/delete/{id}', [\App\Http\Controllers\EventController::class, 'delete'])->name('delete');
Route::get('/send/{id}', [\App\Http\Controllers\EventController::class, 'send'])->name('send');
Route::post('/event/{id}/move', [\App\Http\Controllers\EventController::class, 'move']);

Route::get('/evaluatie/{id}', [\App\Http\Controllers\EvalController::class, 'evaluate'])->name('evaluate');
Route::post('/evaluatie', [\App\Http\Controllers\EvalController::class, 'save'])->name('evaluate');
