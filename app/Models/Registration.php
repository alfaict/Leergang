<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Registration extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'event_id',
        'name',
        'role',
        'organisation',
        'email',
        'phone',
        'need',
        'eval'
    ];

    public static function saveEvalualtion(Request $request)
    {
        $eval = Registration::find($request->id);
        $eval->eval = json_encode($request->eval);
        $eval->save();
    }

}
