<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class Evaluate extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        list($this->user) = DB::select(
            "SELECT `registrations`.*, `events`.`date`, `modules`.`title`
                    FROM `registrations`
                    JOIN `events` ON `registrations`.`event_id` = `events`.`id`
                    JOIN `modules` ON `events`.`module_id` = `modules`.`id`
                    WHERE `registrations`.`id` = {$id};");
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@leergang-regio-opleiders.nl', 'Leergang Regio Opleiders')
                    ->subject('Evaluatie leergang')
                    ->view('emails.evaluate')
                    ->with(['user' => $this->user]);
    }
}
