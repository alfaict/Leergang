<?php

namespace App\Http\Controllers;

use App\Mail\Evaluate;
use App\Models\Event;
use http\QueryString;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use DateTime;

class EventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view($id)
    {
        $currentDate = date('Y-m-d H:i:s');
        $modules = DB::select(
            "SELECT `e`.`id`, `e`.`date`, `m`.`title`, `e`.`module_id`
            FROM `events` `e`
            JOIN `modules` `m` ON `m`.`id` = `e`.`module_id`
            WHERE `e`.`id` != {$id}
            AND `date` >= NOW()
            AND `e`.`module_id` = (SELECT `module_id` FROM `events` WHERE `events`.`id` = {$id})");

        $event = DB::select(
            "SELECT `events`.*, `modules`.`title`, `users`.`name` AS `instructor`,
                    `registrations`.`id` AS `user_id`, `registrations`.`name`, `registrations`.`role`, `registrations`.`organisation`,
                    `registrations`.`email`, `registrations`.`phone`, `registrations`.`need`, `registrations`.`eval`
                    FROM `events`
                    JOIN `modules` ON `events`.`module_id` = `modules`.`id`
                    JOIN `users` ON `events`.`instructor_id` = `users`.`id`
                    LEFT JOIN `registrations` ON `registrations`.`event_id` = `events`.`id`
                    WHERE `events`.`id` = {$id};");
        $emails = [];
        foreach ($event as $user) {
            $emails[] = $user->email;
        }
        $emails = implode(',', $emails);
        return view('pages.event', ['currentdate' => $currentDate, 'modules' => $modules, 'event' => $event[0], 'participants' => $event, 'emails' => $emails]);
    }

    public function new()
    {
        $modules = DB::select("SELECT * FROM `modules`");
        $teachers = DB::select("SELECT * FROM `users`");
        return view('pages.new', ['modules' => $modules, 'teachers' => $teachers]);
    }

    protected function create(Request $request)
    {
        Event::create($request->all());
        header('Location: /tool', true, 307);
        exit;
    }

    public function edit($id)
    {
        list($event) = DB::select("SELECT * FROM `events` WHERE `id` = {$id};");
        $teachers = DB::select("SELECT * FROM `users`");
        return view('pages.edit', ['event' => $event, 'teachers' => $teachers]);
    }

    protected function save(Request $request)
    {
        $event = Event::find($request->event_id);
        $event->instructor_id = $request->instructor_id;
        $event->date = $request->date;
        $event->location = $request->location;
        $event->participants = $request->participants;
        $event->save();
        return view('pages.success', ['message' => 'Training opgeslagen']);
    }

    public function delete($id)
    {
        $event = Event::find($id);
        $event->delete();
        header('Location: /tool', true, 307);
        exit;
    }

    public function send($id)
    {
        $registrations = DB::select("SELECT `id`, `email` FROM `registrations` WHERE `event_id` = {$id} AND `eval` LIKE '[]';");
        foreach ($registrations as $user) {
            Mail::to($user->email)->send(new Evaluate($user->id));
        }
        return view('pages.success', ['message' => 'Evaluatie mail verzonden']);
    }

    public function move($id, Request $request)
    {
        $users = implode(', ', $request->user);
        if (!empty($request->user) && !empty($request->new_event)){
            DB::update("UPDATE `registrations` SET `event_id` = $request->new_event WHERE `id` IN ($users)");
            return redirect()->back()->with('success', 'Gebruiker(s) verplaatst');
        }

    }
}
