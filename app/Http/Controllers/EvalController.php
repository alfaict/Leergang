<?php

namespace App\Http\Controllers;

use App\Models\Registration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EvalController extends Controller
{
    public function evaluate($id, $data = null, $errors = [])
    {
        list($eval) = DB::select(
            "SELECT `registrations`.*, `events`.`date`, `modules`.`title`
                    FROM `registrations`
                    JOIN `events` ON `registrations`.`event_id` = `events`.`id`
                    JOIN `modules` ON `events`.`module_id` = `modules`.`id`
                    WHERE `registrations`.`id` = {$id};");
        if ($eval->eval != '[]') {
            return view('pages.success', ['message' => 'Deze evaluatie is al ingevuld']);
        }
        return view('pages.evaluate', ['eval' => $eval, 'data' => $data, 'error' => $errors]);
    }

    protected function save(Request $request)
    {
        $errors = [];
        foreach ($request->all() as $field => $value) {
            if (empty($value)) {
                $errors[] = $field;
            }
            if (empty($errors)) {
                Registration::saveEvalualtion($request);
                return view('pages.success', ['message' => 'De evaluatie is opgeslagen']);
            }
        }
        return $this->evaluate($request, $errors);
    }

}
