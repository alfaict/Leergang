<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\DB;

class Roles extends Controller
{
    protected static $user;

    public function __construct()
    {
        self::$user = auth()->user();
        if (empty(self::$user->roles)) {
            self::$user = new \stdClass();
            self::$user->roles = '[]';
        }
    }

    public static function userId(): int
    {
        return self::$user->id;
    }

    public static function list()
    {
        self::$user = auth()->user();
        if (self::isAdmin()) {
            $users = DB::select("SELECT * FROM `users`");
            return view('pages.users', ['users' => $users]);
        } else {
            header('Location: /tool', true, 307);
            exit;
        }
    }

    public static function save($userId, $action)
    {
        $user = User::find($userId);
        switch ($action) {
            case 'setTeacher':
                $user->roles = '["Teacher"]';
                break;
            case 'setAdmin':
                $user->roles = '["Admin"]';
                break;
            case 'removeRights':
                $user->roles = '[]';
                break;
            default:
                abort(404);
        }
        $user->save();
        header('Location: /users', true, 307);
        exit;
    }

    public static function isAdmin(): bool
    {
        if (in_array('Admin', json_decode(self::$user->roles, true))) {
            return true;
        }
        return false;
    }

    public static function isTeacher(): bool
    {
        if (in_array('Teacher', json_decode(self::$user->roles, true))) {
            return true;
        }
        return false;
    }
}

new Roles();
