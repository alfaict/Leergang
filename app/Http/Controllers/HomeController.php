<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Roles;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $where = "false";
        if (Roles::isAdmin()) {
            $where = "true";
        } elseif (Roles::isTeacher()) {
            $where = sprintf("`users`.`id` = %s", Roles::userId());
        }
        $events = DB::select(
            "SELECT `events`.*, `modules`.`title`, `users`.`name` AS `instructor`,
                    SUM(IF(`registrations`.`id` IS NOT NULL, 1, 0)) AS `participants`
                    FROM `events`
                    JOIN `modules` ON `events`.`module_id` = `modules`.`id`
                    JOIN `users` ON `events`.`instructor_id` = `users`.`id`
                    LEFT JOIN `registrations` ON `events`.`id` = `registrations`.`event_id`
                    WHERE `events`.`date` >= NOW() AND {$where}
                    GROUP BY `events`.`id`
                    ORDER BY `events`.`date` ASC;");

        $passed = DB::select(
            "SELECT `events`.*, `modules`.`title`, `users`.`name` AS `instructor`,
                    SUM(IF(`registrations`.`id` IS NOT NULL, 1, 0)) AS `participants`
                    FROM `events`
                    JOIN `modules` ON `events`.`module_id` = `modules`.`id`
                    JOIN `users` ON `events`.`instructor_id` = `users`.`id`
                    LEFT JOIN `registrations` ON `events`.`id` = `registrations`.`event_id`
                    WHERE `events`.`date` < NOW() AND {$where}
                    GROUP BY `events`.`id`
                    ORDER BY `events`.`date` DESC;");

        return view('home', ['events' => $events, 'passed' => $passed]);
    }
}
