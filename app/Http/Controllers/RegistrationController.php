<?php

namespace App\Http\Controllers;

use App\Mail\Confirmation;
use App\Mail\Evaluate;
use App\Models\Registration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class RegistrationController extends Controller
{
    public function form($data = null, $errors = [])
    {
        if (empty($data)) {
            $data = (object) ['module_id' => 0];
        }
        $modules = DB::select('SELECT * FROM `modules`');
        $events = [];
        if ($data->module_id > 0) {
            $query = "SELECT `e`.*, SUM(IF(`r`.`id` IS NOT NULL, 1, 0)) AS `registrations` FROM `events` `e`
                        LEFT JOIN `registrations` `r` ON `r`.`event_id` = `e`.`id`
                        WHERE `e`.`module_id` = '{$data->module_id}' AND `e`.`date` > NOW()
                        GROUP BY `e`.`id`
                        HAVING `registrations` < `e`.`participants`;";
            $events = DB::select($query);
        }
        return view('pages.register', ['modules' => $modules, 'events' => $events, 'data' => $data, 'error' => $errors]);
    }

    protected function save(Request $request)
    {
        $errors = [];
        if ($request->event_id > 0) {
            foreach ($request->all() as $field => $value) {
                if (empty($value)) {
                    $errors[] = $field;
                }
            }
            if (empty($errors)) {
                $reg = Registration::create([
                    'module_id' => $request->module_id,
                    'event_id' => $request->event_id,
                    'name' => $request->name,
                    'role' => $request->role,
                    'organisation' => $request->organisation,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'need' => json_encode($request->need),
                    'eval'  => '[]'
                ]);
                Mail::to($request->email)->send(new Confirmation($reg->id));
                return view('pages.success', ['message' => 'U bent succesvol aangemeld voor de module']);
            }
        }
        return $this->form($request, $errors);
    }

}
