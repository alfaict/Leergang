<div class="w-full mb-4">
    <label for="need-1">Heb je ook andere trainingen uit de leergang gevolgd? Zo ja welke?</label>
    <textarea id="need-1" name="need[1]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus></textarea>
</div>
<div class="w-full mb-4">
    <label for="need-2">Begeleid je nu collega's/studenten/stagiaires?</label>
    <textarea id="need-2" name="need[2]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus></textarea>
</div>
<div class="w-full mb-4">
    <label for="need-3">Ben je bekend met de methodiek van didactisch coachen?</label>
    <select id="need-3" name="need[3]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
        <option value=""> - - Kies een optie - - </option>
        <option value="Ja">Ja</option>
        <option value="Nee">Nee</option>
    </select>
</div>
<div class="w-full mb-4">
    <label for="need-4">Ken je "leervraag in beeld", een tool waarmee de student zijn/ haar leervraag helder kan krijgen aan de hand van een vast methode?</label>
    <select id="need-4" name="need[4]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
        <option value=""> - - Kies een optie - - </option>
        <option value="Ja">Ja</option>
        <option value="Nee">Nee</option>
    </select>
</div>
<div class="w-full mb-4">
    <label for="need-5">Wat zijn je verwachtingen van deze training?</label>
    <textarea id="need-5" name="need[5]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus></textarea>
</div>
<div class="w-full mb-4">
    <label for="need-6">Wanneer is voor jou de training geslaagd?</label>
    <textarea id="need-6" name="need[6]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus></textarea>
</div>
