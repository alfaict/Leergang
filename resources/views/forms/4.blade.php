<div class="w-full mb-4">
    <label for="need-1">Ben je bekend met kwalificatiedossiers?</label>
    <select id="need-1" name="need[1]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
        <option value=""> - - Kies een optie - - </option>
        <option value="Ja">Ja</option>
        <option value="Nee">Nee</option>
    </select>
</div>
<div class="w-full mb-4">
    <label for="need-2">Weet je waar beroepstaken aan moeten voldoen?</label>
    <select id="need-2" name="need[2]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
        <option value=""> - - Kies een optie - - </option>
        <option value="Ja">Ja</option>
        <option value="Nee">Nee</option>
    </select>
</div>
<div class="w-full mb-4">
    <label for="need-3">Ben je bekend met valideringsinstrumenten?</label>
    <select id="need-3" name="need[3]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
        <option value=""> - - Kies een optie - - </option>
        <option value="Ja">Ja</option>
        <option value="Nee">Nee</option>
    </select>
</div>
<div class="w-full mb-4">
    <label for="need-4">Wat zijn je verwachtingen van deze training?</label>
    <textarea id="need-4" name="need[4]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus></textarea>
</div>
<div class="w-full mb-4">
    <label for="need-5">Wanneer is voor jou de training geslaagd?</label>
    <textarea id="need-5" name="need[5]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus></textarea>
</div>
