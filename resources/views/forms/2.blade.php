<div class="w-full mb-4">
    <label for="need-1">Heb je ook andere trainingen van de leergang gevolgd? Zo ja welke?</label>
    <textarea id="need-1" name="need[1]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus></textarea>
</div>
<div class="w-full mb-4">
    <label for="need-2">Heb je ervaring met het ontwerpen van een lesprogramma?</label>
    <select id="need-2" name="need[2]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
        <option value=""> - - Kies een optie - - </option>
        <option value="Ja">Ja</option>
        <option value="Nee">Nee</option>
    </select>
</div>
<div class="w-full mb-4">
    <label for="need-3">Heb je ervaring met ontwerpen/ samenstellen van een lesprogramma op maat gemaakt voor een groep of individu?</label>
    <select id="need-3" name="need[3]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
        <option value=""> - - Kies een optie - - </option>
        <option value="Ja">Ja</option>
        <option value="Nee">Nee</option>
    </select>
</div>
<div class="w-full mb-4">
    <label for="need-4">Heb je ervaring met het ontwikkelen / samenstellen van lesmateriaal / leeractiviteiten?</label>
    <select id="need-4" name="need[4]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
        <option value=""> - - Kies een optie - - </option>
        <option value="Ja">Ja</option>
        <option value="Nee">Nee</option>
    </select>
</div>
<div class="w-full mb-4">
    <label for="need-5">Heb je ervaring met het ontwikkelen / samenstellen van lesmateriaal / leeractiviteiten op maat gemaakt voor een groep of individu?</label>
    <select id="need-5" name="need[5]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
        <option value=""> - - Kies een optie - - </option>
        <option value="Ja">Ja</option>
        <option value="Nee">Nee</option>
    </select>
</div>
<div class="w-full mb-4">
    <label for="need-6">Ken je het ontwikkelprincipe backwards design?</label>
    <select id="need-6" name="need[6]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
        <option value=""> - - Kies een optie - - </option>
        <option value="Ja">Ja</option>
        <option value="Nee">Nee</option>
    </select>
</div>
<div class="w-full mb-4">
    <label for="need-7">Heb je lesmateriaal ontwikkeld volgens het ontwikkelprincipe van backwards design?</label>
    <select id="need-7" name="need[7]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
        <option value=""> - - Kies een optie - - </option>
        <option value="Ja">Ja</option>
        <option value="Nee">Nee</option>
    </select>
</div>
<div class="w-full mb-4">
    <label for="need-8">Waar gaat je nieuwsgierigheid vooral naar uit/ wat hoop je minimaal op te halen?</label>
    <textarea id="need-8" name="need[8]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus></textarea>
</div>
<div class="w-full mb-4">
    <label for="need-9">Wanneer is voor jou de training geslaagd?</label>
    <textarea id="need-9" name="need[9]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus></textarea>
</div>
