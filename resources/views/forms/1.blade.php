<div class="w-full mb-4">
    <label for="need-1">Heb je ervaring in het voeren van gesprekken binnen een organisatie in het werkveld?</label>
    <select id="need-1" name="need[1]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
        <option value=""> - - Kies een optie - - </option>
        <option value="Ja">Ja</option>
        <option value="Nee">Nee</option>
    </select>
</div>
<div class="w-full mb-4">
    <label for="need-2">Heb je ervaring het onderzoeken van leervragen bij een organisatie, groep studenten of individuele student?</label>
    <select id="need-2" name="need[2]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
        <option value=""> - - Kies een optie - - </option>
        <option value="Ja">Ja</option>
        <option value="Nee">Nee</option>
    </select>
</div>
<div class="w-full mb-4">
    <label for="need-3">Waar gaat je nieuwsgierigheid vooral naar uit/ wat hoop je minimaal op te halen?</label>
    <textarea id="need-3" name="need[3]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus></textarea>
</div>
<div class="w-full mb-4">
    <label for="need-4">Wanneer is voor jou de training geslaagd?</label>
    <textarea id="need-4" name="need[4]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus></textarea>
</div>
