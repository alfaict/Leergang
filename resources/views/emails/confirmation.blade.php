<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>
</head>
<body style="font-family:sans-serif;">
    <div style="width:100%;background-color:#e8330f;">
        <p style="color:white;padding:1.5rem;font-size:1.25rem;">{{ config('app.name', 'Laravel') }}</p>
    </div>
    <div style="margin:auto;padding:1.5rem;">
        <p>Beste {{ $user->name }},</p>
        <p>Hartelijk dank voor je aanmelding voor de training {{ $user->title }} op {{ date('j-m-Y', strtotime($user->date)) }} om {{ date('H:i', strtotime($user->date)) }}.</p>
        <p>Let op: deze datum en het tijdstip zijn <em>voorlopig</em>! Doorgang is afhankelijk van het aantal deelnemers.</p>
        <p>Enige tijd van tevoren hoor je of de training <em>definitief</em> doorgaat of dat een alternatieve datum wordt aangeboden. Ook ontvang je dan verdere praktische informatie.</p>
    </div>
</body>
</html>
