<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>
</head>
<body style="font-family:sans-serif;">
    <div style="width:100%;background-color:#e8330f;">
        <p style="color:white;padding:1.5rem;font-size:1.25rem;">{{ config('app.name', 'Laravel') }}</p>
    </div>
    <div style="margin:auto;padding:1.5rem;">
        <p>Beste {{ $user->name }},</p>
        <p>Op {{ date('j-m-Y', strtotime($user->date)) }} heb je de training {{ $user->title }} gevolgd. Graag horen we van je hoe je de training hebt ervaren. Over de onderwerpen in de evaluatie ontvangen we graag feedback.</p>
        <a href="{{ config('app.url', 'http://localhost') }}/evaluatie/{{ $user->id }}" style="display:block;background-color:#e8330f;border-radius:.25rem;width:100%;max-width:20rem;padding:.5rem;margin:1.5rem auto;text-align:center;text-decoration:none;color:white;">Evaluatie invullen</a>
    </div>
</body>
</html>
