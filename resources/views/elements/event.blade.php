<tr class="item divide-x divide-gray-500 hover:bg-white">
    <td class="p-1">{{ $event->title }}</td>
    <td class="p-1"><a href="/event/{{ $event->id }}">{{ date('d-m-Y H:i', strtotime($event->date)) }}-{{ date('H:i', strtotime($event->date.' +3 HOUR')) }}</a></td>
    <td class="p-1">{{ $event->location }}</td>
    <td class="p-1">{{ $event->instructor }}</td>
    <td class="p-1">{{ $event->participants }}</td>
@if(\App\Http\Controllers\Roles::isAdmin())
        <th class="w-1/12 text-center p-1">
            <a href="/edit/{{ $event->id }}" class="text-gray-700 hover:text-gray-500">&#9999;</a>
            &nbsp;&nbsp;&nbsp;
            <a href="#" data-confirm="Weet je zeker dat je de training '{{ $event->title }} van {{ date('d-m-Y H:i', strtotime($event->date)) }}-{{ date('H:i', strtotime($event->date.' +3 HOUR')) }} wil verwijderen?'" data-id="{{ $event->id }}" class="delete text-red-700 hover:text-red-500">&cross;</a>
        </th>
    @endif
</tr>
