@extends('layouts.app')

@section('content')
    <h2>Toekomstige trainingen</h2>
    <table id="events" class="table-fixed w-full divide-y divide-gray-500 mb-4 mx-auto">
        <thead>
        <tr>
            <th class="w-3/12 p-1">Module</th>
            <th class="w-2/12 p-1">Datum</th>
            <th class="w-3/12 p-1">Locatie</th>
            <th class="w-2/12 p-1">Docent</th>
            <th class="w-1/12 p-1">Deelnemers</th>
        @if(\App\Http\Controllers\Roles::isAdmin())
                <th class="w-1/12 text-base text-center p-1">&#9999;&nbsp;&nbsp;&nbsp;&nbsp;&cross;</th>
            @endif
        </tr>
        </thead>
        <tbody class="divide-y divide-gray-500">
        @foreach($events as $event)
            @include('elements.event', ['event' => $event])
        @endforeach
        </tbody>
    </table>
    @if(\App\Http\Controllers\Roles::isAdmin())
    <a href="/event/new" class="block bg-brand rounded shadow w-full max-w-xs mx-auto p-2 mb-4 text-base text-white text-center hover:underline">Nieuwe datum toevoegen</a>
    @endif
    <h2>Geschiedenis trainingen</h2>
    <table id="events" class="table-fixed w-full divide-y divide-gray-500 mb-4 mx-auto">
        <thead>
        <tr>
            <th class="w-3/12 p-1">Module</th>
            <th class="w-2/12 p-1">Datum</th>
            <th class="w-3/12 p-1">Locatie</th>
            <th class="w-2/12 p-1">Docent</th>
            <th class="w-1/12 p-1">Deelnemers</th>
        @if(\App\Http\Controllers\Roles::isAdmin())
                <th class="w-1/12 text-base text-center p-1">&#9999;&nbsp;&nbsp;&nbsp;&nbsp;&cross;</th>
            @endif
        </tr>
        </thead>
        <tbody class="divide-y divide-gray-500">
        @foreach($passed as $event)
            @include('elements.event', ['event' => $event])
        @endforeach
        </tbody>
    </table>
@endsection
