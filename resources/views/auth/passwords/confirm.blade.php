@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('password.confirm') }}" class="max-w-md mx-auto">
        @csrf
        <div class="w-full mb-2">
            <label for="password">Bevestig je wachtwoord om door te gaan</label>
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror  flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" name="password" required autocomplete="current-password">
        </div>
        @foreach ($errors->all() as $error)
            <span class="block w-full text-brand text-center font-semibold my-2" role="alert">{{ $error }}</span>
        @endforeach
        <div class="w-full mb-2">
            <button type="submit" class="block bg-brand rounded shadow w-full max-w-xs mx-auto p-2 text-base text-white text-center hover:underline">Wachtwoord bevestigen</button>
        </div>
        @if (Route::has('password.request'))
            <div class="w-full">
                <a class="block w-full text-brand text-center text-sm font-italic" href="{{ route('password.request') }}">Wachtwoord vergeten?</a>
            </div>
        @endif
    </form>
@endsection
