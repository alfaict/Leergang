@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('password.update') }}" class="max-w-md mx-auto">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="w-full mb-2">
            <label for="email">E-mailadres</label>
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
        </div>
        <div class="w-full mb-2">
            <label for="password">Wachtwoord</label>
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror  flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" name="password" required autocomplete="new-password">
        </div>
        <div class="w-full mb-4">
            <label for="password-confirm">Herhaal wachtwoord</label>
            <input id="password-confirm" type="password" class="form-control @error('password') is-invalid @enderror  flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" name="password_confirmation" required autocomplete="new-password">
        </div>
        @foreach ($errors->all() as $error)
            <span class="block w-full text-brand text-center font-semibold my-2" role="alert">{{ $error }}</span>
        @endforeach
        <div class="w-full mb-2">
            <button type="submit" class="block bg-brand rounded shadow w-full max-w-xs mx-auto p-2 text-base text-white text-center hover:underline">Wachtwoord wijzigen</button>
        </div>
    </form>
@endsection
