@extends('layouts.app')

@section('content')
    @if (session('status'))
        <div class="block bg-green-500 rounded shadow w-full max-w-md mx-auto p-2 text-base text-white mb-4" role="alert">{{ session('status') }}</div>
    @endif
    <form method="POST" action="{{ route('password.email') }}" class="max-w-md mx-auto">
        @csrf
        <div class="w-full mb-4">
            <label for="email">E-mailadres</label>
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
        </div>
        @foreach ($errors->all() as $error)
            <span class="block w-full text-brand text-center font-semibold my-2" role="alert">{{ $error }}</span>
        @endforeach
        <div class="w-full mb-2">
            <button type="submit" class="block bg-brand rounded shadow w-full max-w-xs mx-auto p-2 text-base text-white text-center hover:underline">Stuur wachtwoord reset link</button>
        </div>
    </form>
@endsection
