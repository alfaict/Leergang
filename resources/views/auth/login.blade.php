@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('login') }}" class="max-w-md mx-auto">
        @csrf
        <div class="w-full mb-2">
            <label for="email">E-mailadres</label>
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
        </div>
        <div class="w-full mb-2">
            <label for="password">Wachtwoord</label>
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror  flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" name="password" required autocomplete="current-password">
        </div>
        <div class="w-full pt-2 mb-2 mx-1 form-check">
            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
            <label class="form-check-label" for="remember">Inloggegevens onthouden</label>
        </div>
        @foreach ($errors->all() as $error)
            <span class="block w-full text-brand text-center font-semibold my-2" role="alert">{{ $error }}</span>
        @endforeach
        <div class="w-full mb-2">
            <button type="submit" class="block bg-brand rounded shadow w-full max-w-xs mx-auto p-2 text-base text-white text-center hover:underline">Inloggen</button>
        </div>
        @if (Route::has('password.request'))
            <div class="w-full">
                <a class="block w-full text-brand text-center text-sm font-italic" href="{{ route('password.request') }}">Wachtwoord vergeten?</a>
            </div>
        @endif
    </form>
@endsection
