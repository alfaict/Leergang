@extends('layouts.app')

@section('content')
    @if (session('resent'))
        <div class="block bg-green-500 rounded shadow w-full max-w-md mx-auto p-2 text-base text-white mb-4" role="alert">Er is een link verzonden om je e-mailadres te bevestigen</div>
    @endif
    <form method="POST" action="{{ route('verification.resend') }}" class="max-w-md mx-auto">
        @csrf
        <div class="w-full mb-2">
            <button type="submit" class="block bg-brand rounded shadow w-full max-w-xs mx-auto p-2 text-base text-white text-center hover:underline">Verstuur bevestigingslink</button>
        </div>
    </form>
@endsection
