@extends('layouts.app')

@section('content')
    @if(empty($events))
    <div class="max-w-2xl mx-auto mb-4">
        <p>In voorbereiding op onze training willen we alvast een inventarisatie doen m.b.t. de ontwikkelingen die te maken hebben met het onderwerp Leven Lang Ontwikkelen (LLO). Wellicht ben je al gestart met LLO activiteiten of misschien zit je nog in de oriëntatiefase. Wij willen graag meedenken met het bedrijfsleven op dit onderwerp. Onderstaand staat kort beschreven welke visie wij hebben op Leven Lang Ontwikkelen. Daaronder een filmpje welke een idee geven wat LLO voor scholen en het bedrijfsleven inhoudt. Om onze training goed voor te bereiden wil ik je vragen onderstaande vragen te beantwoorden over LLO.</p>
        <p><strong>Visie</strong>
        <br>Regio opleiders draagt bij aan het succes van organisaties in de MBO sector, de wendbaarheid van de medewerkers te vergroten d.m.v. opleiden. Oftewel partijen die aangesloten zijn bij Regio Opleiders biedt opleidingen en trainingen om de kennis en vaardigheden van medewerkers in diverse branches te vergroten en up to date houden.</p>
        <p>Regio Opleiders heeft een herkenbaar en inspirerend LLO programma. Dit programma komt tegemoet aan de vraag van alumni, professionals, medewerkers in de sectoren, en werkzoekenden naar voldoende kennis en kunde om hun bestaande of toekomstige positie te versterken op de arbeidsmarkt, dan wel een positie te werven op de arbeidsmarkt.</p>
        <p>Het inspirerend aanbod van Regio Opleiders bestaat uit scholing (diplomagericht of certificaatgericht), workshops, seminars, lezingen waarin kennis en vaardigheden worden geboden die het leven lang ontwikkelen ondersteunen. We ontwikkelen dit aanbod in nauwe samenwerking met het bedrijfsleven, brancheorganisaties, leveranciers van trainingen/modules en alumni.</p>
        <iframe src="https://www.youtube.com/embed/jbJqG_EqyhI" title="Leven Lang Ontwikkelen" frameborder="0" allow="" class="w-full h-72 max-w-lg mx-auto"></iframe>
    </div>
    @endif
    <form id="aanmelden" method="POST" action="{{ route('aanmelden') }}" class="max-w-md mx-auto">
        @csrf
        <div class="w-full mb-4">
            <label for="module_id">Module</label>
            <select name="module_id" id="module_id" class="form-control @if(in_array('module_id', $error)) is-invalid @endif flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
                <option value=""> - - Kies een optie - - </option>
                @foreach($modules as $module)
                    <option value="{{ $module->id }}" @if($module->id == $data->module_id) selected @endif>{{ $module->title }}</option>
                @endforeach
            </select>
        </div>
        @if(!empty($events))
            <div class="w-full mb-2">
                <p>{{ $modules[$data->module_id-1]->description }}</p>
            </div>
            <div class="w-full mb-2">
                <label for="event_id">Datum &amp; locatie</label>
                <select name="event_id" id="event_id" class="form-control @if(in_array('event_id', $error)) is-invalid @endif flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
                    <option value=""> - - Kies een optie - - </option>
                    @foreach($events as $event)
                        <option value="{{ $event->id }}" @if($event->id == $data->event_id) selected @endif>{{ date('d-m-Y H:i', strtotime($event->date)) }}-{{ date('H:i', strtotime($event->date . ' +3 HOUR')) }} | {{ $event->location }}</option>
                    @endforeach
                </select>
            </div>
            <div class="w-full mb-2">
                <label for="name">Naam</label>
                <input id="name" type="text" class="form-control @if(in_array('name', $error)) is-invalid @endif flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" name="name" value="{{ $data->name }}" required autocomplete="name" autofocus>
            </div>
            <div class="w-full mb-2">
                <label for="role">Rol / Functie</label>
                <input id="role" type="text" class="form-control @if(in_array('role', $error)) is-invalid @endif flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" name="role" value="{{ $data->role }}" required autocomplete="role" autofocus>
            </div>
            <div class="w-full mb-2">
                <label for="organisation">Organisatie</label>
                <input id="organisation" type="text" class="form-control @if(in_array('organisation', $error)) is-invalid @endif flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" name="organisation" value="{{ $data->organisation }}" required autocomplete="organisation" autofocus>
            </div>
            <div class="w-full mb-2">
                <label for="email">E-mailadres</label>
                <input id="email" type="email" class="form-control @if(in_array('email', $error)) is-invalid @endif flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" name="email" value="{{ $data->email }}" required autocomplete="email" autofocus>
            </div>
            <div class="w-full mb-4">
                <label for="phone">Telefoonnummer</label>
                <input id="phone" type="text" class="form-control @if(in_array('phone', $error)) is-invalid @endif flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" name="phone" value="{{ $data->phone }}" required autocomplete="phone" autofocus>
            </div>
            @foreach ($errors->all() as $error)
                <span class="block w-full text-brand text-center font-semibold my-2" role="alert">{{ $error }}</span>
            @endforeach
            @if(!empty($error))
                <span class="block w-full text-brand text-center font-semibold my-2" role="alert">Niet alle velden zijn correct ingevuld</span>
            @endif
            <div class="w-full mb-4">
                <p>Om de training zoveel mogelijk op maat te maken doen we vooraf een behoeftepeiling.</p>
            </div>
            @include('forms.'.$data->module_id)
            <div class="w-full mb-2">
                <button type="submit" class="block bg-brand rounded shadow w-full max-w-xs mx-auto p-2 text-base text-white text-center hover:underline">Aanmelden</button>
            </div>
        @endif
    </form>
@endsection
