@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <form id="evaluate" method="POST" action="{{ route('evaluate') }}" class="max-w-md mx-auto">
        @csrf
        <input type="hidden" name="id" value="{{ $eval->id }}">
        <p class="mb-2">Op {{ date('j-m-Y', strtotime($eval->date)) }} heb je de training {{ $eval->title }} gevolgd. Graag horen we van je hoe je de training hebt ervaren.</p>
        <div class="w-full mb-2">
            <label for="eval-1">Heeft de training aan je verwachting voldaan?</label>
            <select id="eval-1" name="eval[1]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
                <option value=""> - - Kies een optie - - </option>
                <option value="Ja">Ja</option>
                <option value="Gedeeltelijk">Gedeeltelijk</option>
                <option value="Nee">Nee</option>
                <option value="Boven verwachting">Boven verwachting</option>
            </select>
        </div>
        <div class="w-full mb-2">
            <label for="eval-2">Waarom heeft de training wel of niet aan je verwachting voldaan?</label>
            <textarea id="eval-2" name="eval[2]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus></textarea>
        </div>
        <div class="w-full mb-2">
            <label for="eval-3">Kwam de training overeen met de doelen die aan het begin zijn besproken?</label>
            <textarea id="eval-3" name="eval[3]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus></textarea>
        </div>
        <p class="mb-2">Wil je de volgende vragen beantwoorden met het aantal sterren dat je het waard vond?</p>
        <div class="w-full mb-2 grid grid-cols-3 gap-2">
            <label for="eval-4" class="col-span-2">De Trainingsinhoud</label>
            <div id="eval-4" class="stars">
                <input type="radio" name="eval[4]" value="1" required autofocus><i></i>
                <input type="radio" name="eval[4]" value="2" required autofocus><i></i>
                <input type="radio" name="eval[4]" value="3" required autofocus><i></i>
                <input type="radio" name="eval[4]" value="4" required autofocus><i></i>
                <input type="radio" name="eval[4]" value="5" required autofocus><i></i>
            </div>
        </div>
        <div class="w-full mb-2 grid grid-cols-3 gap-2">
            <label for="eval-5" class="col-span-2">De kennis van de trainer</label>
            <div id="eval-5" class="stars">
                <input type="radio" name="eval[5]" value="1" required autofocus><i></i>
                <input type="radio" name="eval[5]" value="2" required autofocus><i></i>
                <input type="radio" name="eval[5]" value="3" required autofocus><i></i>
                <input type="radio" name="eval[5]" value="4" required autofocus><i></i>
                <input type="radio" name="eval[5]" value="5" required autofocus><i></i>
            </div>
        </div>
        <div class="w-full mb-2 grid grid-cols-3 gap-2">
            <label for="eval-6" class="col-span-2">Enthousiasme van de trainer</label>
            <div id="eval-6" class="stars">
                <input type="radio" name="eval[6]" value="1" required autofocus><i></i>
                <input type="radio" name="eval[6]" value="2" required autofocus><i></i>
                <input type="radio" name="eval[6]" value="3" required autofocus><i></i>
                <input type="radio" name="eval[6]" value="4" required autofocus><i></i>
                <input type="radio" name="eval[6]" value="5" required autofocus><i></i>
            </div>
        </div>
        <div class="w-full mb-2 grid grid-cols-3 gap-2">
            <label for="eval-7" class="col-span-2">Interactie met de groep en trainer</label>
            <div id="eval-7" class="stars">
                <input type="radio" name="eval[7]" value="1" required autofocus><i></i>
                <input type="radio" name="eval[7]" value="2" required autofocus><i></i>
                <input type="radio" name="eval[7]" value="3" required autofocus><i></i>
                <input type="radio" name="eval[7]" value="4" required autofocus><i></i>
                <input type="radio" name="eval[7]" value="5" required autofocus><i></i>
            </div>
        </div>
        <div class="w-full mb-2 grid grid-cols-3 gap-2">
            <label for="eval-8" class="col-span-2">De onderdelen van de training sloten goed aan bij mijn werksituatie</label>
            <div id="eval-8" class="stars">
                <input type="radio" name="eval[8]" value="1" required autofocus><i></i>
                <input type="radio" name="eval[8]" value="2" required autofocus><i></i>
                <input type="radio" name="eval[8]" value="3" required autofocus><i></i>
                <input type="radio" name="eval[8]" value="4" required autofocus><i></i>
                <input type="radio" name="eval[8]" value="5" required autofocus><i></i>
            </div>
        </div>
        <div class="w-full mb-2 grid grid-cols-3 gap-2">
            <label for="eval-9" class="col-span-2">De opgedane kennis kan ik toepassen in mijn werksituatie</label>
            <div id="eval-9" class="stars">
                <input type="radio" name="eval[9]" value="1" required autofocus><i></i>
                <input type="radio" name="eval[9]" value="2" required autofocus><i></i>
                <input type="radio" name="eval[9]" value="3" required autofocus><i></i>
                <input type="radio" name="eval[9]" value="4" required autofocus><i></i>
                <input type="radio" name="eval[9]" value="5" required autofocus><i></i>
            </div>
        </div>
        <div class="w-full mb-2 grid grid-cols-3 gap-2">
            <label for="eval-10" class="col-span-2">De voorbeelden van de trainer sloten aan bij mijn werksituatie</label>
            <div id="eval-10" class="stars">
                <input type="radio" name="eval[10]" value="1" required autofocus><i></i>
                <input type="radio" name="eval[10]" value="2" required autofocus><i></i>
                <input type="radio" name="eval[10]" value="3" required autofocus><i></i>
                <input type="radio" name="eval[10]" value="4" required autofocus><i></i>
                <input type="radio" name="eval[10]" value="5" required autofocus><i></i>
            </div>
        </div>
        <div class="w-full mb-2 grid grid-cols-3 gap-2">
            <label for="eval-11" class="col-span-2">Ik heb ideeën op kunnen doen van andere organisatie</label>
            <div id="eval-11" class="stars">
                <input type="radio" name="eval[11]" value="1" required autofocus><i></i>
                <input type="radio" name="eval[11]" value="2" required autofocus><i></i>
                <input type="radio" name="eval[11]" value="3" required autofocus><i></i>
                <input type="radio" name="eval[11]" value="4" required autofocus><i></i>
                <input type="radio" name="eval[11]" value="5" required autofocus><i></i>
            </div>
        </div>
        <div class="w-full mb-2">
            <label for="eval-12">Zijn er onderdelen die je gemist hebt in de training en waarover je een vervolgtraining zou willen volgen?</label>
            <textarea id="eval-12" name="eval[12]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus></textarea>
        </div>
        <div class="w-full mb-4">
            <label for="eval-13">Heb je nog opmerkingen die je kwijt wilt, maar waarop je hierboven geen antwoord kon geven?</label>
            <textarea id="eval-13" name="eval[13]" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus></textarea>
        </div>
        @foreach ($errors->all() as $error)
            <span class="block w-full text-brand text-center font-semibold my-2" role="alert">{{ $error }}</span>
        @endforeach
        @if(!empty($error))
            <span class="block w-full text-brand text-center font-semibold my-2" role="alert">Niet alle velden zijn correct ingevuld</span>
        @endif
        <div class="w-full mb-2">
            <button type="submit" class="block bg-brand rounded shadow w-full max-w-xs mx-auto p-2 text-base text-white text-center hover:underline">Opslaan</button>
        </div>
    </form>
@endsection
