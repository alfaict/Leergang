@extends('layouts.app')

@section('content')
    <p class="font-semibold text-lg text-green-700 text-center">{{ $message }}</p>
@endsection
