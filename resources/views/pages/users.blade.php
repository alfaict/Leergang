@extends('layouts.app')

@section('content')
    <table id="event" class="table-fixed w-full divide-y divide-gray-500 mb-4 mx-auto">
        <thead>
        <tr>
            <th class="w-3/12 p-1">Docent</th>
            <th class="w-3/12 p-1">E-mail</th>
            <th class="w-2/12 p-1">Rol</th>
            <th class="w-4/12 p-1">Wijzigen</th>
        </tr>
        </thead>
        <tbody class="divide-y divide-gray-500">
        @foreach($users as $user)
        @php $roles = json_decode($user->roles) @endphp
        <tr class="item divide-x divide-gray-500">
            <td class="p-1">{{ $user->name }}</td>
            <td class="p-1">{{ $user->email }}</td>
            @if(in_array('Admin', $roles))
                <td class="p-1">Admin</td>
                <td class="p-1 flex">
                    <a href="/user/{{ $user->id }}/removeRights" class="block bg-brand rounded shadow w-40 mx-auto text-base text-white text-center hover:underline">Verwijder rechten</a>
                </td>
            @elseif(in_array('Teacher', $roles))
                <td class="p-1">Docent</td>
                <td class="p-1 flex">
                    <a href="/user/{{ $user->id }}/setAdmin" class="block bg-brand rounded shadow w-40 mx-auto text-base text-white text-center hover:underline">Maak Admin</a>
                    <a href="/user/{{ $user->id }}/removeRights" class="block bg-brand rounded shadow w-40 mx-auto text-base text-white text-center hover:underline">Verwijder rechten</a>
                </td>
            @else
                <td class="p-1">&nbsp;</td>
                <td class="p-1 flex">
                    <a href="/user/{{ $user->id }}/setTeacher" class="block bg-brand rounded shadow w-40 mx-auto text-base text-white text-center hover:underline">Maak Docent</a>
                    <a href="/user/{{ $user->id }}/setAdmin" class="block bg-brand rounded shadow w-40 mx-auto text-base text-white text-center hover:underline">Maak Admin</a>
                </td>
            @endif
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
@endsection
