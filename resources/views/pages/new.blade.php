@extends('layouts.app')

@section('content')
    <form id="new_event" method="POST" action="{{ route('new_event') }}" class="max-w-md mx-auto">
        @csrf
        <div class="w-full mb-2">
            <label for="module_id">Training toevoegen</label>
            <select name="module_id" id="module_id" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
                <option value=""> - - Kies een optie - - </option>
                @foreach($modules as $module)
                    <option value="{{ $module->id }}">{{ $module->title }}</option>
                @endforeach
            </select>
        </div>
        <div class="w-full mb-2">
            <label for="instructor_id">Docent</label>
            <select name="instructor_id" id="instructor_id" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
                <option value=""> - - Kies een optie - - </option>
                @foreach($teachers as $teacher)
                    <option value="{{ $teacher->id }}">{{ $teacher->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="w-full mb-2">
            <label for="date">Datum</label>
            <input name="date" id="date" type="datetime-local" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
        </div>
        <div class="w-full mb-2">
            <label for="location">Locatie</label>
            <input name="location" id="location" type="text" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
        </div>
        <div class="w-full mb-4">
            <label for="participants">Aantal deelnemers</label>
            <input name="participants" id="participants" type="number" min="0" step="1" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
        </div>
        <div class="w-full mb-2">
            <button type="submit" class="block bg-brand rounded shadow w-full max-w-xs mx-auto p-2 text-base text-white text-center hover:underline">Opslaan</button>
        </div>
    </form>
@endsection
