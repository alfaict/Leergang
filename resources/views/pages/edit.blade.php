@extends('layouts.app')

@section('content')
    <form id="new_event" method="POST" action="{{ route('save_event') }}" class="max-w-md mx-auto">
        @csrf
        <div class="w-full mb-2">
            <p class="font-semibold">Training {{ $event->id }} van {{ date('d-m-Y H:i', strtotime($event->date)) }}-{{ date('H:i', strtotime($event->date.' +3 HOUR')) }} wijzigen</p>
            <input type="hidden" name="event_id" value="{{ $event->id }}">
        </div>
        <div class="w-full mb-2">
            <label for="instructor_id">Docent</label>
            <select name="instructor_id" id="instructor_id" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
                <option value=""> - - Kies een optie - - </option>
                @foreach($teachers as $teacher)
                    <option value="{{ $teacher->id }}" @if($event->instructor_id == $teacher->id) selected @endif>{{ $teacher->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="w-full mb-2">
            <label for="date">Datum</label>
            <input name="date" id="date" type="datetime-local" value="{{ date('Y-m-d', strtotime($event->date)) }}T{{ date('H:i', strtotime($event->date)) }}" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
        </div>
        <div class="w-full mb-2">
            <label for="location">Locatie</label>
            <input name="location" id="location" type="text" value="{{ $event->location }}" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
        </div>
        <div class="w-full mb-4">
            <label for="participants">Aantal deelnemers</label>
            <input name="participants" id="participants" type="number" min="0" step="1" value="{{ $event->participants }}" class="form-control flex-grow border rounded shadow py-2 pl-2 leading-tight focus:outline-none" required autofocus>
        </div>
        <div class="w-full mb-2">
            <button type="submit" class="block bg-brand rounded shadow w-full max-w-xs mx-auto p-2 text-base text-white text-center hover:underline">Opslaan</button>
        </div>
    </form>
@endsection
