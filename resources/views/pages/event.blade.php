@extends('layouts.app')

@section('content')
    <table id="event" class="table-fixed w-full divide-y divide-gray-500 mb-4 mx-auto">
        <thead>
        <tr>
            <th class="w-4/12 p-1">Module</th>
            <th class="w-2/12 p-1">Datum</th>
            <th class="w-3/12 p-1">Locatie</th>
            <th class="w-2/12 p-1">Docent</th>
            <th class="w-1/12 p-1">Deelnemers</th>
        </tr>
        </thead>
        <tbody class="divide-y divide-gray-500">
        <tr class="item divide-x divide-gray-500">
            <td class="p-1">{{ $event->title }}</td>
            <td class="p-1">{{ date('d-m-Y H:i', strtotime($event->date)) }}-{{ date('H:i', strtotime($event->date.' +3 HOUR')) }}</td>
            <td class="p-1">{{ $event->location }}</td>
            <td class="p-1">{{ $event->instructor }}</td>
            <td class="p-1">{{ count($participants) }}</td>
        </tr>
        </tbody>
    </table>
    @if(\App\Http\Controllers\Roles::isAdmin())
        <div class="flex">
            <a href="/send/{{ $event->id }}" class="block bg-brand rounded shadow w-full max-w-xs p-2 mb-4 mr-4 text-base text-white text-center hover:underline">Verstuur evaluatie mail</a>
            <a href="#" id="copy" onclick="navigator.clipboard.writeText('{{ $emails }}');" class="block bg-gray-700 rounded shadow w-full max-w-xs p-2 mb-4 text-base text-white text-center hover:underline">Kopieer e-mailadressen</a>
        </div>
    @endif
    <form action="/event/{{ $event->id }}/move" method="post" class="w-full divide-y divide-gray-500 mb-4 mx-auto">
        @csrf
        <div class="grid grid-cols-12">
            <p class="font-semibold">Selecteer</p>
            <p class="col-span-3 font-semibold">Deelnemer</p>
            <p class="col-span-3 font-semibold">Behoefte</p>
            <p class="col-span-5 font-semibold">Evaluatie</p>
        </div>
        @foreach($participants as $user)
            <div class="grid grid-cols-12 py-2">
                <input type="checkbox" name="user[]" value="{{ $user->user_id }}">
                <div class="col-span-3">{{ $user->name }}
                    <br>{{ $user->role }}
                    <br>{{ $user->organisation }}
                    <br>{{ $user->email }}
                    <br>{{ $user->phone }}
                </div>
                <ul class="col-span-3">
                    @if(!empty($user->need))
                        @foreach(json_decode($user->need) as $i => $need)
                            <li>{{ $i }} - {{ $need }}</li>
                        @endforeach
                    @endif
                    <br>&nbsp;
                </ul>
                <ul class="col-span-5 grid grid-cols-2">
                    @if(!empty($user->eval))
                        @foreach(json_decode($user->eval) as $i => $eval)
                            <li>{{ $i }} - {{ $eval }}</li>
                        @endforeach
                    @endif
                    <br>&nbsp;
                </ul>
            </div>
        @endforeach
        <div class="w-full pt-2 mb-2">
            <select name="new_event" class="form-control w-full max-w-xs border rounded shadow py-2 my-2 leading-tight focus:outline-none" autofocus required>
                <option value=""> - Kies een nieuwe datum - </option>
                @foreach($modules as $option)
                    <option value="{{ $option->id }}">{{ $option->date }}</option>
                @endforeach
            </select>
            <button type="submit" class="block bg-brand rounded shadow w-full max-w-xs p-2 text-base text-white text-center hover:underline">Opslaan</button>
        </div>
        @if (Session::has('success'))
            <div class="block bg-green-700 rounded shadow w-full max-w-xs p-2 text-base text-white text-center">
                {{ Session::get('success') }}
            </div>
        @endif
        </form>
    </div>
@endsection
